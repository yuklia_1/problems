<?php
function calculate($arr, $len)
{
    $inArray = function ($ar, $value) use ($len) {
        for ($i = 0; $i < $len; $i++) {
            if ($ar[$i] == $value) {
                return true;
            }
        }

        return false;
    };

    $i = 0;
    $missed = [];
    $duplicates = [];
    $min = $arr[$i];
    $max = $arr[$i];

    for (; $i < $len;) {
        $current = $arr[$i];
        if ($arr[$i] < $min) {
            $min = $arr[$i];
        }

        if ($arr[$i] > $max) {
            $max = $arr[$i];
        }

        if (isset($duplicates[$current])) {
            $duplicates[$current]++;
        } else {
            $duplicates[$current] = 1;
        }
        $i++;

    }

    for ($j = $min; $j <= $max; $j++) { //range
        if (!$inArray($arr, $j)) {
            $missed[] = $j;
        }
    }

    echo sprintf("Range is %s to %s", $min, $max) . "</br>";
    echo "Missing Numbers: </br>";
    foreach ($missed as $num) {
        echo $num . "</br>";
    }
    echo "Duplicate Numbers: </br>";
    foreach ($duplicates as $key => $num) {
        if ($num >= 2) {
            echo sprintf('%d appears %d times', $key, $num) . "</br>";
        }
    }
}

$test = [-3, 1, -5, 10, 3, -5, 0, 3, 1, 1];
calculate($test, count($test));