<?php

try {
    $host = 'localhost';
    $dbname  = 'problem2';
    $user = 'root';
    $pass = '';
    $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);

    //user
    $STH = $DBH->query("select p.name as personName, p.id as personId, m.name as majorName from person as p
    left join majors as m ON (m.id=p.majorId)");
    $STH->setFetchMode(PDO::FETCH_OBJ);
    $person= $STH->fetchAll();

    //winter
    $STH = $DBH->query("select p.name as personName, po.name as point, po.point as pointValue, p.id as personId, s.name as subjectName from grades as e
     left join points as po ON (po.id=e.pointId)
     left join person as p ON (p.id=e.userId)
     left join subjects as s ON (s.id=e.subjectId)
     where (MONTH(e.est_date) = 12 or MONTH(est_date) <= 2) and YEAR(e.est_date)=2013");
    $STH->setFetchMode(PDO::FETCH_OBJ);
    $winter = $STH->fetchAll();

    //fall
     $STH = $DBH->query("select p.name as personName, po.name as point, po.point as pointValue, s.name as subjectName from grades as e
     left join person as p ON (p.id=e.userId)
     left join points as po ON (po.id=e.pointId)
     left join subjects as s ON (s.id=e.subjectId)
     where (MONTH(est_date) between 9 and 11) and YEAR(e.est_date)=2012");
    $STH->setFetchMode(PDO::FETCH_OBJ);
    $fall = $STH->fetchAll();


    //spring
    $STH = $DBH->query("select p.name as personName,po.name as point, po.point as pointValue, s.name as subjectName from grades as e
     left join person as p ON (p.id=e.userId)
     left join points as po ON (po.id=e.pointId)
     left join subjects as s ON (s.id=e.subjectId)
     where (MONTH(est_date) between 3 and 5) and YEAR(e.est_date)=2012");
    $STH->setFetchMode(PDO::FETCH_OBJ);
    $spring = $STH->fetchAll();

    include "table.phtml";

}
catch(PDOException $e) {
    echo $e->getMessage();
}